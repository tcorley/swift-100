//
//  ViewController.swift
//  Project1-TapCounter
//
//  Created by Tyler Corley on 8/30/16.
//  Copyright © 2016 tcorley. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var counter: UILabel!
    var count:Int = 0
    let gradientLayer = CAGradientLayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.green()
        gradientLayer.frame = self.view.bounds
        
        let color1 = UIColor.blue().cgColor
        let color2 = UIColor.green().cgColor
        
        gradientLayer.colors = [color1, color2]
        gradientLayer.locations = [0.0, 1.0]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        
        // Do any additional setup after loading the view, typically from a nib.
        self.counter.text = String(self.count)
    }

    @IBAction func tapToIncrement(_ sender: AnyObject) {
        self.count += 1
        self.counter.text = String(self.count)
    }
    
    @IBAction func tapToReset(_ sender: AnyObject) {
        self.count = 0
        self.counter.text = "0"
    }

}

